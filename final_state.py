
class State:
    def __init__(self):
        self.initialize()

    def initialize(self):
        # list to store program history
        self.backup_storage = [{'initial': {}}]
        # index of current program in storage
        self.current_index = 0
        # current active state name
        self.current_state = 'initial'
        # current active graph name
        self.current_graph = None

        # when done parsing this variable will hold our AST
        self.AST = None

state = State()
