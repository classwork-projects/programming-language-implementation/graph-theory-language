# A tree walker to interpret graph language
import copy
import sys

import graph_algorithms
from final_state import state
from grammar_stuff import assert_match

#########################################################################
# graph functions
#########################################################################
def seq(node):
    (SEQ, stmt, stmt_list) = node
    assert_match(SEQ, 'seq')

    walk(stmt)
    walk(stmt_list)


#########################################################################
def nil(node):
    (NIL,) = node
    assert_match(NIL, 'nil')

    # do nothing!
    pass


#########################################################################
# fill in here
#########################################################################
def graph_stmt(node):
    (GRAPH, STRING, command) = node
    assert_match(GRAPH, 'graph')

    state.current_graph = walk(STRING)
    state.backup_storage.insert(state.current_index + 1, copy.deepcopy(state.backup_storage[state.current_index]))
    state.current_index += 1
    curr_state = state.backup_storage[state.current_index][state.current_state]
    if state.current_graph not in curr_state:
        curr_state[state.current_graph] = {}
    walk(command)

#########################################################################
# removes given graph or state
#########################################################################
def delete_stmt(node):
    (DELETE, type, item) = node
    assert_match(DELETE, 'delete')

    value = walk(item)
    if type == 'graph':
        del state.backup_storage[state.current_index][state.current_state][value]
    elif type == 'state':
        del state.backup_storage[state.current_index][value]
    else:
        raise ValueError("unknown delete item: " + type)



#########################################################################
def end_stmt(node):
    (END,) = node
    assert_match(END, 'end')

    sys.exit()


#########################################################################
def string_label(node):
    (STRING, value) = node
    assert_match(STRING, 'string')

    return value


#########################################################################
# saves program state
#########################################################################
def save_stmt(node):
    (SAVE, STRING) = node
    assert_match(SAVE, 'save')

    value = walk(STRING)
    state.backup_storage[state.current_index][value] = copy.deepcopy(state.backup_storage[state.current_index][state.current_state])
    state.current_state = value


#########################################################################
# loads program state
#########################################################################
def load_stmt(node):
    (LOAD, STRING) = node
    assert_match(LOAD, 'load')

    state.current_state = walk(STRING)


#########################################################################
# merges graphs together
#########################################################################
def merge_stmt(node):
    (MERGE, STRING1, STRING2, STRING3) = node
    assert_match(MERGE, 'merge')

    graph1 = walk(STRING1)
    graph2 = walk(STRING2)
    new_graph = walk(STRING3)
    curr_state = state.backup_storage[state.current_index][state.current_state]
    curr_state[new_graph] = copy.deepcopy(curr_state[graph1])
    curr_state[new_graph].update(curr_state[graph2])


#########################################################################
# revert program state to before last command
#########################################################################
def undo_stmt(node):
    (UNDO,) = node
    assert_match(UNDO, 'undo')

    if state.current_index > 1:
        state.current_index -= 1
    else:
        print('ERROR: No operations to undo.')


#########################################################################
# reverse undo operation
#########################################################################
def redo_stmt(node):
    (REDO,) = node
    assert_match(REDO, 'redo')

    if state.current_index != len(state.backup_storage)-1:
        state.current_index += 1
    else:
        print('ERROR: No undo operation to redo.')

    pass

# value_list
#########################################################################
# fill in here
#########################################################################
def change_command(node):
    (CHANGE, STRING1, STRING2) = node
    assert_match(CHANGE, 'change')

    graph1 = walk(STRING1)
    graph2 = walk(STRING2)
    curr_graph = state.backup_storage[state.current_index][state.current_state][state.current_graph]
    if graph1 in curr_graph:
        curr_graph[graph2] = curr_graph.pop(graph1)
    else:
        print('ERROR: Graph ' + graph2 + ' does not exist')


#########################################################################
# fill in here
#########################################################################
def print_command(node):
    (PRINT, type) = node
    assert_match(PRINT, 'print')

    curr_graph = state.backup_storage[state.current_index][state.current_state][state.current_graph]
    if type == 'edges':
        temp = [(key, node) for key, value in curr_graph.items() for node in value]
        print(temp)
    elif type == 'all':
        print(curr_graph)


#########################################################################
# add node to graph
#########################################################################
def node_command(node):
    assert_match(node[0], 'node')
    (NODE, labels) = node
    value = walk(labels)
    curr_graph = state.backup_storage[state.current_index][state.current_state][state.current_graph]
    if value not in curr_graph:
        curr_graph[value] = []
    else:
        print('ERROR: A node with this name already exists')


#########################################################################
# add edge between nodes
#########################################################################
def edge_command(node):
    (EDGE, STRING1, STRING2) = node
    assert_match(EDGE, 'edge')

    curr_graph = state.backup_storage[state.current_index][state.current_state][state.current_graph]
    node1 = walk(STRING1)
    node2 = walk(STRING2)
    if node1 in curr_graph and node2 in curr_graph:
        curr_graph[node1].append(node2)
    else:
        print('ERROR: Only two existing nodes can be connected.')


#########################################################################
# fill in here
#########################################################################
def degree_command(node):
    (DEGREE, STRING) = node
    assert_match(DEGREE, 'degree')

    node = walk(STRING)
    curr_graph = state.backup_storage[state.current_index][state.current_state][state.current_graph]
    adjacent_list = []
    for key, value in curr_graph.items():
        if key == node and len(value) > 0 and value not in adjacent_list:
            adjacent_list.append(value)
        elif node in value and key not in adjacent_list:
            adjacent_list.append(key)
    print('Node ' + node + ' has degree ' + str(len(adjacent_list)))


#########################################################################
# generates path for specifically provided nodes(vertices)
#########################################################################
def paths_command(node):
        (PATHS, type, STRING1, STRING2) = node
        assert_match(PATHS, 'paths')

        node1 = walk(STRING1)
        node2 = walk(STRING2)
        curr_graph = state.backup_storage[state.current_index][state.current_state][state.current_graph]
        if type == 'path':
            print(graph_algorithms.find_shortest_path(curr_graph, node1, node2))
        elif type == 'allpaths':
            print(graph_algorithms.find_all_paths(curr_graph, node1, node2))

#########################################################################
# walk
#########################################################################
def walk(node):
    # node format: (TYPE, [child1[, child2[, ...]]])
    type = node[0]

    if type in dispatch_dict:
        node_function = dispatch_dict[type]
        return node_function(node)
    else:
        raise ValueError("walk: unknown tree node type: " + type)


# a dictionary to associate tree nodes with node functions
dispatch_dict = {
    'seq': seq,  # done
    'nil': nil,  # done
    'graph': graph_stmt,  # done
    'delete': delete_stmt,  # done
    'save': save_stmt,  # function created
    'load': load_stmt,  # function created
    'merge': merge_stmt,  # function created
    'undo': undo_stmt,  # function created
    'redo': redo_stmt,  # function created
    'change': change_command,  # function created
    'print': print_command,  # function created
    'node': node_command,  # function created
    'edge': edge_command,  # function created
    'degree': degree_command,  # function created
    'paths': paths_command,  # function created
    'string': string_label  # done
}
