# grammar for final project

from ply import yacc
from final_state import state
from final_lex import tokens, lexer

#########################################################################
# grammar rules with embedded actions
#########################################################################
def p_prog(p):
    '''
    prog : stmt_list
    '''
    state.AST = p[1]

#########################################################################
def p_stmt_list(p):
    '''
    stmt_list : stmt stmt_list
              | stmt
    '''
    if (len(p) == 3):
        p[0] = ('seq', p[1], p[2])
    elif (len(p) == 2):
        p[0] = p[1]

#########################################################################
def p_stmt(p):
    '''
    stmt : GRAPH label command
         | DELETE STATE label
         | DELETE GRAPH label
         | SAVE label
         | LOAD label
         | MERGE label label label
         | UNDO
         | REDO
    '''
    if p[1] == 'graph':
        p[0] = ('graph', p[2], p[3])
    elif p[1] == 'delete':
        p[0] = ('delete', p[2], p[3])
    elif p[1] == 'save':
        p[0] = ('save', p[2])
    elif p[1] == 'load':
        p[0] = ('load', p[2])
    elif p[1] == 'merge':
        p[0] = ('merge', p[2], p[3], p[4])
    elif p[1] == 'change':
        p[0] = ('change', p[2], p[3])
    elif p[1] == 'undo':
        p[0] = ('undo',)
    elif p[1] == 'redo':
        p[0] = ('redo',)
    else:
        raise ValueError("unexpected symbol {}".format(p[1]))

#########################################################################
def p_command_node(p):
    '''
    command : NODE label
    '''

    p[0] = ('node', p[2])

#########################################################################
def p_command_EDGE(p):
    '''
    command : EDGE label label
    '''

    p[0] = ('edge', p[2], p[3])

#########################################################################
def p_command_CHANGE(p):
    '''
    command : CHANGE label label
    '''

    p[0] = ('change', p[2], p[3])

#########################################################################
def p_command_print(p):
    '''
    command : PRINT DEGREE label
           | PRINT PATH label label
           | PRINT ALLPATHS label label
           | PRINT EDGES
           | PRINT ALL
    '''
    length = len(p)
    if length == 5:
        p[0] = ('paths', p[2], p[3], p[4])
    elif length == 4:
        p[0] = ('degree', p[3])
    else:
        p[0] = ('print', p[2])

#########################################################################
def p_string(p):
    '''
    label : STRING
    '''
    p[0] = ('string', p[1])

#########################################################################
def p_error(t):
    print("Syntax error at '%s'" % t.value)

#########################################################################
# build the parser
#########################################################################
parser = yacc.yacc()