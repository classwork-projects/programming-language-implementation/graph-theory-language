# Graph Theory Language

### Introduction
This language is as a graph theory oriented language which has a wide variety of purposes and use cases within the graph theory field. At a glance, a user can generate a complex graph by creating nodes and linking each node by edges. The user can also print assortments of the graphs, filtered by either edges or paths. Due to these features, it might be helpful for somebody interested in storing a library of unique graphs. Storing road maps and routes, social media applications associating nodes as people and edges are a friend relationship, and storing data for graph theory problems like “The Marriage Problem” are distinct use cases for this.  
<br />

### Architecture
The implementation of this language is largely an extension of our previous assignments, building on state management and our use of Lex-Yacc. We stored the program states as a list of dictionaries for our undo and redo operations. We also implemented nested dictionaries, since the graph dictionaries are stored within their respective program state dictionary. This setup takes advantage of the benefits of key, value pairing, with the keys in the state dictionary being the graph names, paired with the values containing the graph contents. And the keys in the graph dictionary are the nodes of the graph; with its respective value as a list of nodes connected to that node by an edge.  
<br />

### Challenges
The most challenging aspect of our implementation was managing program states and undo/redo operations. The complexity of making a record of each operation and also managing multiple program states became a very complex problem. A limitation of our implementation is that undo/redo operations are only supported for ‘graph’ commands. If we were to do it over I think a more organized and structured grammar would of made the language easier to build. Many times we manually check what sort of data the command is manipulating instead of calling that specific operation from the dispatch dictionary. If we had more time we would of came of with a method of tracking each command to make undo/redo operations more robust. A method of printing graphs using a GUI would also make this language very powerful.  
<br />

### Conclusion
This language provides a very useful method of creating and managing graphs. The ability to manage multiple graphs and different program states required using the program state class in a way that we have not done on previous assignments. This made our language very powerful, and given all the applications of graph theory this language could be worked into a useful language with many real world applications. Overall, it is a good representation of the power of graph theory and the Python Lex-Yacc library.  
<br />

### Usage
**Language Sample 1**  
```
prog1 = \
'''
graph new_graph node a
graph new_graph node b
graph new_graph node x
graph new_graph node y
graph new_graph node z
graph new_graph edge a x
graph new_graph edge x y
graph new_graph edge a z
graph new_graph edge b y
graph secondGraph node c
graph secondGraph node d
graph secondGraph node o
graph secondGraph node p
graph secondGraph edge c d
graph secondGraph edge c o
graph secondGraph edge p d
graph secondGraph edge o d

graph new_graph print all
graph secondGraph print all

merge new_graph secondGraph combinedGraph
graph combinedGraph print all
'''
parser.parse(prog1)
interp(prog1)  
```

```
{'a': ['x', 'z'], 'b': ['y'], 'x': ['y'], 'y': [], 'z': []}
{'c': ['d', 'o'], 'd': [], 'o': ['d'], 'p': ['d']}
{'a': ['x', 'z'], 'b': ['y'], 'x': ['y'], 'y': [], 'z': [], 'c': ['d', 'o'], 'd': [], 'o': ['d'], 'p': ['d']}
```
<br />

**Language Sample 2**  
```  
prog2 = \
'''
graph new_graph node a
graph new_graph node b
graph new_graph node c
save new_graph_created_state
graph new_graph node d
graph new_graph edge a b
graph new_graph edge b c
graph new_graph edge c d
graph new_graph edge d a
graph new_graph print path a d
'''

parser.parse(prog2)
interp(prog2)
```

`['a', 'b', 'c', 'd']`
<br />

**Language Sample 3**  
```
prog3 = \
'''
graph new_graph node a
graph new_graph node b
graph new_graph node c
save new_graph_created_state
graph new_graph node d
graph new_graph node z
undo
graph new_graph edge a b
graph new_graph edge b c
graph new_graph edge c d
graph new_graph edge d a
graph new_graph print degree a
graph new_graph print degree z

graph new_graph print path a c

graph new_graph print allpaths a d

graph new_graph print edges

graph new_graph change a new_a

graph new_graph print all

save new_graph_created_state
'''
parser.parse(prog3)
interp(prog3)
```

```
Node a has degree 2
Node z has degree 0
['a', 'b', 'c']
[['a', 'b', 'c', 'd']]
[('a', 'b'), ('b', 'c'), ('c', 'd'), ('d', 'a')]
{'b': ['c'], 'c': ['d'], 'd': ['a'], 'new_a': ['b']}
```
<br />

**Language Sample 4**  
```
prog4 = \
'''
graph new_graph node a
graph new_graph node f
graph new_graph node g
graph new_graph node d
graph new_graph node z
graph new_graph node x
graph new_graph edge a f
graph new_graph edge f d
graph new_graph edge f z
graph new_graph edge z a
graph new_graph edge d a
graph new_graph edge d x
graph new_graph print degree a
graph new_graph print path z x
graph new_graph print edges
save new_graph_created_state

'''
parser.parse(prog4)
interp(prog4)
```

```
Node a has degree 3
['z', 'a', 'f', 'd', 'x']
[('a', 'f'), ('f', 'd'), ('f', 'z'), ('d', 'a'), ('d', 'x'), ('z', 'a')]
```
<br />

**Language Sample 5**   
```
prog5 = \
'''
graph new_graph node a
save new_graph_created_state

graph secondGraph node c
save secondGraph_created_state

delete graph new_graph
delete graph secondGraph
load new_graph_created_state
load secondGraph_created_state

save no_graphs_state
delete state no_graphs_state
'''
parser.parse(prog5)
interp(prog5)
```




