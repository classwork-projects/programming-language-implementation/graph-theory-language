# Lexer for Cuppa2

from ply import lex

reserved = {
    'graph'         : 'GRAPH',
    'save'          : 'SAVE',
    'load'          : 'LOAD',
    'merge'         : 'MERGE',
    'change'        : 'CHANGE',
    'undo'          : 'UNDO',
    'redo'          : 'REDO',
    'print'         : 'PRINT',
    'node'          : 'NODE',
    'edge'          : 'EDGE',
    'degree'        : 'DEGREE',
    'path'          : 'PATH',
    'edges'         : 'EDGES',
    'all'           : 'ALL',
    'state'         : 'STATE',
    'delete'        : 'DELETE',
    'allpaths'      : 'ALLPATHS'
}

tokens = [ 'STRING',] + list(reserved.values())

t_ignore = ' \t'

def t_STRING(t):
    r'[a-zA-Z_][a-zA-Z_0-9]*'
    t.type = reserved.get(t.value,'STRING') # strip the quotes
    return t

def t_COMMENT(t):
    r'\’.*'
    pass

def t_NEWLINE(t):
    r'\n'
    pass

def t_error(t):
    print("Illegal character %s" % t.value[0])
    t.lexer.skip(1)

# build the lexer
lexer = lex.lex(debug=0)

